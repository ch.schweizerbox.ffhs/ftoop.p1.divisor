package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CalculateDivisorMain {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		if (args.length != 3) {
			System.out.println("Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>");
			System.exit(1);
		}

		long von = 12; // Long.parseLong(args[0]);
		long bis = 358; // Long.parseLong(args[1]);
		int threads = 3; // Integer.parseInt(args[2]);

		// Wie gross muss der Bereich sein, den jeder Thread berechnen muss?
		int bereichsgroesse = berechneBereichsGroesse(von, bis, threads);

		// Startet den ExecutorService mit der gewuenschten Anzahl Threads.
		ExecutorService execService = Executors.newFixedThreadPool(threads);

		// In diese Liste werden die einzelnen Resultate der verschiedenen
		// Threads abgef�llt.
		List<DivisorResult> resultList = new ArrayList<DivisorResult>();

		for (int i = 0; i <= threads - 1; i++) {
			// bestimmt die Bereichsgr�sse f�r jeden Thread.
			long bistemp = von + bereichsgroesse;
			if (bistemp > bis) {
				bistemp = bis;
			}
			// Callable Objekt f�r die Berechnung
			Callable<DivisorResult> worker = new CalculateDivisor(von, bistemp);
			Future<DivisorResult> future = execService.submit(worker);
			resultList.add(future.get());
			System.out.println("Resultatzahl ist: " + future.get().result);
			System.out.println("Anzahl Divisoren sind: " + future.get().countDiv +"\n");
			von += bereichsgroesse + 1;
			Thread.sleep(600);
		}
		execService.shutdown();

		// Alle Resultate der verschiedenen Threads gegen�berstellen und das Beste ermitteln:
		DivisorResult result = ermittleBestenWert(resultList);
		System.out.println("**************************************************************");
		System.out.println(result);
		System.out.println("**************************************************************");

	}

	/**
	 * @param von
	 *            Beginn des Berechnungsbereiches
	 * @param bis
	 *            Ende des Berechnungsbereiches
	 * @param threads
	 *            Anzahl gew�nschter Threads
	 * @return Gibt die gr�sse des Bereichs zur�ck den jeder Thread berechnen
	 *         muss.
	 */
	public static int berechneBereichsGroesse(double von, double bis, int threads) {

		double bereichsgroesse = (bis - von) / threads;
		bereichsgroesse = Math.ceil(bereichsgroesse * 1) / 1;
		// System.out.println(bereichsgroesse);
		return (int) bereichsgroesse;

	}

	/**
	 * @param resultList Liste mit den ermittelten Anzahl Divisoren & Resultaten (DivisorResult)
	 * @return Den besten Wert (Meiste Divisoren)
	 */
	public static DivisorResult ermittleBestenWert(List<DivisorResult> resultList) {
		double result = 0;
		int index = 0;
		for (int i = 0; i < resultList.size(); i++) {
			if (result < resultList.get(i).getCountDiv()) {
				result = resultList.get(i).getCountDiv();
				index = i;
			}

		}
		return resultList.get(index);
	}

}
