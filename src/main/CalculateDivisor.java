package main;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Das folgende Programm soll aus einem vorgegebene Interval von Long-Zahlen die
 * Zahl zurückgeben, die die meisten Divisoren hat. Sollte es mehrere solche
 * Zahlen geben, so soll die kleinste dieser Zahlen ausgegeben werden.
 * 
 * Die Berechnung soll in n Threads stattfinden, die via Executor Framework
 * gesteuert werden, und sich das Problem aufteilen - jeder Thread soll eine
 * Teilmenge des Problems lösen. Verwenden Sie bitte einen FixedThreadPool und
 * implementieren Sie die Worker als Callable.
 * 
 * @author ble
 * 
 */
public class CalculateDivisor implements Callable<DivisorResult>{

	double von, bis;
	private int id;
	private static int counter;
	

	/**
	 * @param von
	 *            untere Intervallgrenze
	 * @param bis
	 *            obere Intervallgrenze
	 * @param id
	 *            Abzahl der Threads, auf die das Problem aufgeteilt werden soll
	 */
	public CalculateDivisor(double von, double bis) {
		this.von = von;
		this.bis = bis;
		this.id = counter++;
	}


	
	/**
	 * Berechnungsroutine
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	DivisorResult calculate() throws InterruptedException, ExecutionException {

		double hoechsteZahl = von;
		double hoechsterCounter = 0;
		
		for (double k = von; k <= bis; k++) {

			int count = 0;
			for (double i = 1; i <= k; i++) {
				double rest = k % i;
				if (rest == 0) {
					++count;
				}
			if(hoechsterCounter < count){
//				System.out.print("<Thread "+id+"> neue H�chstZahl! ");
//				System.out.println("<Thread " +id+ ">- "+k + " - ("+count+" Divisoren)");
				hoechsterCounter = count;
				hoechsteZahl = k;
//				Thread.sleep(100);
			}
			}
		}

		return new DivisorResult(hoechsteZahl, hoechsterCounter);
		
	}


	@Override
	public DivisorResult call() throws Exception {
		System.out.printf("<Thread %d (Bereich %s bis %s)>\n",id,von,bis);
		DivisorResult dr = calculate();
		return dr; 
	}

}

/**
 * Hält das Ergebnis einer Berechnung
 * 
 * @author bele
 * 
 * 
 */
class DivisorResult {
	// das eigentlich ergebnis - die Zahl mit der max. Anzahl von Divisoren
	double result;

	// Anzahl der Divisoren von Result
	double countDiv;

	public DivisorResult(double r, double c) {
		result = r;
		countDiv = c;
	}

	public double getResult() {
		return result;
	}

	public double getCountDiv() {
		return countDiv;
	}

	@Override
	public String toString() {
		return "Zahl mit maximaler Anzahl Divisoren: " + result + " ("
				+ countDiv + " Divisoren)";
	}

}
